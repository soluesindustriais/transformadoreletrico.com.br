<? $h1 = "Transformador de força"; $title  = "Transformador de força"; $desc = "Receba diversos comparativos de $h1, você encontra nos resultados das pesquisas do Soluções Industriais, receba diversos comparativos hoje mesmo com dezenas de distribuidores"; $key  = "Comprar transformador de força,Transformadores de força"; include('inc/head.php');  ?>
</head>

<body>
     <?php include('inc/topo.php');?>
    <div class="wrapper">
        <main>
            <div class="content">
                <section><?=$caminhoinformacoes?><br class="clear" />
                    <h1><?=$h1?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?=$url?>imagens/mpi/transformador-de-forca-01.jpg"
                                title="<?=$h1?>" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/transformador-de-forca-01.jpg" title="<?=$h1?>"
                                    alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/transformador-de-forca-02.jpg"
                                title="Comprar transformador de força" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/transformador-de-forca-02.jpg"
                                    title="Comprar transformador de força" alt="Comprar transformador de força"></a><a
                                href="<?=$url?>imagens/mpi/transformador-de-forca-03.jpg"
                                title="Transformadores de força" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/transformador-de-forca-03.jpg"
                                    title="Transformadores de força" alt="Transformadores de força"></a></div><span
                            class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível
                            livremente na internet</span>
                        <hr />
                        <p>O <strong><a style="color: #ff4d52;" href="<?=$url?>transformadores-eletricos">transformador</a> de força</strong> é um equipamento destinado a transmitir energia
                            elétrica ou potência elétrica de um circuito a outro provocando tensões, correntes e
                            modificando os valores das impedâncias elétricas de um circuito elétrico. Esse material é um
                            dispositivo que funciona através da indução de corrente de acordo com os princípios do
                            eletromagnetismo.</p>
                        <p>Com o eletromagnetismo, é possível criar uma corrente elétrica em um circuito uma vez que
                            esse seja submetido a um campo magnético variável, e por conta da necessidade dessa variação
                            no fluxo magnético os transformadores só podem funcionar se houver corrente alternada.</p>
                        <h2>Componentes do transformador de força</h2>
                        <p>O produto possui o enrolamento, ou seja, várias bolinhas que são feitas de cobre eletrolítico
                            e recebem uma camada de verniz sintético para que seja isolado. Assim como o núcleo, feito
                            de um material silício o responsável por transferir a corrente induzida no enrolamento
                            primário para o enrolamento secundário. Esses dois componentes são conhecidos como parte
                            ativa, e os demais fazem parte dos acessórios complementares.</p>
                        <h2>Principais vantagens do equipamento</h2>
                        <ul>
                            <li class="li-mpi">Durabilidade;</li>
                            <li class="li-mpi">Custo-benefício;</li>
                            <li class="li-mpi">Alta tecnologia;</li>
                            <li class="li-mpi">Entre inúmeras outras.</li>
                        </ul>
                        <p>Entre em contato para solicitar seu orçamento.</p>
                    </article>
                     <?php include('inc/coluna-mpi.php');?><br class="clear">
                     <?php include('inc/busca-mpi.php');?>
                     <?php include('inc/form-mpi.php');?>
                     <?php include('inc/regioes.php');?>
                </section>
            </div>
        </main>
    </div>
     <?php include('inc/footer.php');?>
</body>

</html>