<? $h1 = "Transformadores elétricos"; $title  = "Transformadores elétricos"; $desc = "Faça um orçamento de $h1, veja as melhores indústrias, solicite uma cotação agora com aproximadamente 100 fornecedores ao mesmo tempo"; $key  = "Comprar transformadores elétricos,Transformador elétrico"; include('inc/head.php');  ?>
</head>

<body>
     <?php include('inc/topo.php');?>
    <div class="wrapper">
        <main>
            <div class="content">
                <section><?=$caminhoinformacoes?><br class="clear" />
                    <h1><?=$h1?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?=$url?>imagens/mpi/transformadores-eletricos-01.jpg"
                                title="<?=$h1?>" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/transformadores-eletricos-01.jpg" title="<?=$h1?>"
                                    alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/transformadores-eletricos-02.jpg"
                                title="Comprar transformadores elétricos" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/transformadores-eletricos-02.jpg"
                                    title="Comprar transformadores elétricos"
                                    alt="Comprar transformadores elétricos"></a><a
                                href="<?=$url?>imagens/mpi/transformadores-eletricos-03.jpg"
                                title="Transformador elétrico" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/transformadores-eletricos-03.jpg"
                                    title="Transformador elétrico" alt="Transformador elétrico"></a></div><span
                            class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível
                            livremente na internet</span>
                        <hr />

                        <p>Todo aparelho ou equipamento que necessite de energia diferente das que há no
                            estabelecimento, irá necessitar de um <strong>transformadores elétricos</strong>. Há
                            disponibilidade de transformadores e tensões com potências diversas para melhor atendê-los.
                            Antes de realizar a compra do equipamento, faça uma pesquisa das vendedoras, de seus
                            históricos no mercado, o que a mesma têm a oferecer quanto aos testes do equipamento, e
                            profissionais responsáveis pela fabricação dos mesmos.</p>
                        <p>Para se comprar <strong>transformadores elétricos</strong> é necessário saber a especificação
                            do equipamento e do local em que o mesmo será ligado. Com essas definições, é possível obter
                            segurança para que não haja ter problema futuros, como a queima do transformador.</p>
                        <h2>Realizações desses transformadores</h2>

                        <h2>Tipos e utilizações do Transformador Elétrico</h2>
                        <p>Há vários tipos de <strong>transformador elétrico</strong> que são utilizados nos dias de
                            hoje, sendo que eles são
                            classificados levando em conta uma série de critérios: finalidade, tipo, número de fases e
                            material do núcleo, entre
                            outras.</p>
                        <p>De acordo com a finalidade nós podemos realçar os seguintes:</p>
                        <ul>
                            <li>Transformadores para ar condicionado,</li>
                            <li>Transformadores de energia,</li>
                            <li>Transformadores de corrente,</li>
                            <li>Transformadores de distribuição,</li>
                            <li>Transformadores de força,</li>
                            <li>Transformadores de potência.</li>
                        </ul>

                        <p>Quanto ao tipo de material do núcleo nós podemos lembrar os seguintes: núcleos de ar e o
                            ferromagnético.</p>
                        <p>No que diz respeito ao tipo, nós podemos focar a presença dos seguintes tipos:</p>
                        <div style="display: flex;justify-content: center;width: 507px;margin: auto;">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/byVn1Sbk_YU"
                                frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen=""></iframe>

                        </div>
                        <ul>
                            <li> <strong>transformador </strong>
                                <strong>autotransformador</strong> ou <strong>Auto Trafo</strong>,</li>
                            <li>E com dois ou mais enrolamentos.</li>
                        </ul>
                        <p>De acordo com o número de fases podemos lembrar-nos dos seguintes tipos:</p>


                        <ul>
                            <li> <i>Transformador monofásicos,</i> </li>
                            <li> <i>Transformadores trifásico,</i> </li>
                            <li> <i>Transformadores polifásico.</i> </li>
                        </ul>
                        <p>Os transformadores de potência são simplesmente os mais
                            conhecidos justamente por conta da sua importância.</p>
                        <p>Já que eles têm como principal função rebaixar ou ampliar a tensão, além de também elevar ou
                            reduzir a corrente
                            dentro de um circuito.</p>
                        <p>Isto é feito de maneira a não alterar a potência de um circuito elétrico, e este
                            <strong>transformador
                                elétrico</strong> também podem ser dividido em pelo menos dois grupos:</p>
                        <ul>
                            <li>Transformador de força,</li>
                            <li>Transformador de distribuição.</li>
                        </ul>
                        <p>Desta forma, é possível garantir, entre outras coisas, que a tensão seja controlada antes de
                            ser entregue ao seu alvo
                            final, por questões de segurança e de praticidade, dando a noção da sua importância.</p>
                        <p>Portanto, agora você já sabe muito mais sobre o transformador elétrico e conhece todos os
                            seus tipos e todas as suas
                            finalidades e utilizações.</p>
                        <p>Quer saber mais sobre o <strong>transformador elétrico,</strong> sua aplicação e
                            diferenciais?</p>
                        <ul>
                            <li class="li-mpi">São preparados para diversas atividades humanas consideradas
                                fundamentais;</li>
                            <li class="li-mpi">Uma das atividades é a condução de energia elétrica;</li>
                            <li class="li-mpi">O transformador trabalha em abaixar ou aumentar a tensão em variados
                                lugares de distribuição da eletricidade;</li>
                            <li class="li-mpi">São equipamentos relevantes, econômicos e eficientes;</li>
                            <li class="li-mpi">Reduz custos na transmissão de energia elétrica em grandes distâncias.
                            </li>
                        </ul>
                        <p>Solicite o orçamento do transformador agora mesmo, clicando no botão!</p>
                    </article>
                     <?php include('inc/coluna-mpi.php');?><br class="clear">
                     <?php include('inc/busca-mpi.php');?>
                     <?php include('inc/form-mpi.php');?>
                     <?php include('inc/regioes.php');?>
                </section>
            </div>
        </main>
    </div>
     <?php include('inc/footer.php');?>
</body>

</html>