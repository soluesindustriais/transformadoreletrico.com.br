<?php

$CategMenu[] = array(
  'pasta' => 'categorias',
  'icone' => 'fa fa-list-ol',
  'titulo' => 'Categorias',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);
$CategMenu[] = array(
  'pasta' => 'blog',
  'icone' => 'fa fa-quote-left',
  'titulo' => 'Blog',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'banner',
  'icone' => 'fa fa-desktop',
  'titulo' => 'Banner',
  'url' => array(
    // array('pagina' => 'configuracoes', 'titulo' => 'Configurações'),
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar'),
  ),
);

$CategMenu[] = array(
  'pasta' => 'newsletter',
  'icone' => 'fa fa-envelope',
  'titulo' => 'Newsletter',
  'url' => array(
// array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar'),
// array('pagina' => 'index', 'titulo' => 'Enviar')
  )
);