<div class="card-group">
    <?php
        foreach ($vetKey as $key => $vetor) :
            $mpiUrl   = $vetor['url'];
            $mpiTitle = $vetor['key'];
            $mpiDesc  = $vetor['desc'];
            $mpiCover = glob('imagens/informacoes/'.$mpiUrl.'-{,[0-9]}[0-9].jpg', GLOB_BRACE); // Default Cover
            $mpiAltCover = 'imagens/informacoes/'.$mpiUrl.'-'.$vetor['cover'].'.jpg';  // Alternative cover

            if ($vetor['cat_id'] == $mpiCatId): ?>
                <div class="card card--mpi">
                    <a class="card__link" rel="nofollow" href="<?=$url.$mpiUrl?>" title="<?=$mpiTitle?>">
                        <img class="card__image" src="<?= $url.(($vetor['cover'] && $vetor['cover'] != ' ') ? $mpiAltCover : $mpiCover[0]) ?>" alt="<?=$mpiTitle?>" title="<?=$mpiTitle?>">
                        <h3 class="card__title" itemprop="name"><?=$mpiTitle?></h3>
                    </a>
                </div>
        <?php endif;
    endforeach; ?>
</div>