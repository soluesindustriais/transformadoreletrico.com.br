<? $h1 = "Categoria - Transformadores"; $title  = "Categoria - Transformadores"; $desc = "Orce $h1, conheça os melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo"; $key  = ""; include('inc/head.php');?>
<link rel="stylesheet" href="<?=$url?>css/thumbnails.css">
</head>

<body> <?php include('inc/topo.php');?> <div class="wrapper">
        <main role="main">
            <div class="content">
                <section> <?=$caminhotransformadores?>
                    <?php include_once('inc/transformadores/transformadores-buscas-relacionadas.php');?><br
                        class="clear" />
                    <h1><?=$h1?></h1>
                    <article class="full">
                        <p>O mercado de <?=$h1?> é amplo e conta com produtos e serviços que podem ser úteis em diversas
                            aplicações. No Soluções Industriais, portal especializado na geração de negócios para o
                            mercado B2B, é possível encontrar as melhores empresas que atuam nesse segmento.</p>
                        <p>Além de receber um orçamento, você também poderá esclarecer suas dúvidas referentes ao
                            assunto. Saiba mais sobre <?=$h1?> e faça uma cotação.</p>
                        <ul class="thumbnails-2">
                            <?php include_once('inc/transformadores/transformadores-categoria.php');?> </ul>
                    </article> <br class="clear">
                </section>
            </div>
        </main>
    </div><!-- .wrapper --> <?php include('inc/footer.php');?> </body>

</html>