<?
$nomeSite			= 'Transformador Elétrico';
$slogan				= 'Conte com os melhores fornecedores de Transofrmadores do Brasil';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
	$url = $http . "://" . $host . "/";
} else {
	$url = $http . "://" . $host . $dir["dirname"] . "/";
}

$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-122304892-1';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php', '', $urlPagina);
$urlPagina 			== "index" ? $urlPagina = "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
$isMobile =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
//Breadcrumbs
$caminho 			= '
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" itemprop="url" href="' . $url . '" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
</div>
';
$caminho2	= '
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="' . $url . '" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		<a href="' . $url . 'transformadores-categoria" title="Transformadores - Categoria" class="category" itemprop="url"><span itemprop="title"> Transformadores - Categoria </span></a> »
		<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';
$caminhoBread 			= '
<div class="wrapper">
	<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
		<a rel="home" itemprop="url" href="' . $url . '" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
		<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
	</div>
	<h1>' . $h1 . '</h1>
</div>
</div>
';


$caminhoinformacoes = '
<div class="breadcrumb" id="breadcrumb  ">
    <div class="wrapper">
        <div class="bread__row">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $url . '" itemprop="item" title="Home">
        <span itemprop="name"> Home   </span>
      </a>
      <meta itemprop="position" content="1" />
    </li>
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a  href="' . $url . 'informacoes" itemprop="item" title="Informações">
        <span itemprop="name">Informações   </span>
      </a>
      <meta itemprop="position" content="2" />
    </li>

    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <span itemprop="name">' . $h1 . '</span>
      <meta itemprop="position" content="3" />
    </li>
  </ol>
</nav>
</div>
</div>
</div>
  ';



$caminhotransformadores = '
<div class="breadcrumb" id="breadcrumb  ">
    <div class="wrapper">
        <div class="bread__row">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $url . '" itemprop="item" title="Home">
        <span itemprop="name"> Home   </span>
      </a>
      <meta itemprop="position" content="1" />
    </li>
	<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
	<a  href="' . $url . 'transformadores-categoria" itemprop="item" title="Transformadores - Categoria">
	  <span itemprop="name">Transformadores - Categoria   </span>
	</a>
	<meta itemprop="position" content="2" />
  </li>
    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <span itemprop="name">' . $h1 . '</span>
      <meta itemprop="position" content="3" />
    </li>
  </ol>
</nav>
</div>
</div>
</div>
  ';
