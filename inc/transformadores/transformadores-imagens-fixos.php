<div class="wrap grid">
    <div class="col-md-6">
        <div class="picture-legend picture-center"><a class="lightbox" href="<?= $url ?>imagens/transformadores/transformadores-01.jpg" title="<?= $h1 ?>" target="_blank"><img class="lazyload" data-src="<?= $url ?>imagens/transformadores/thumbs/transformadores-01.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>" /></a><strong>Imagem ilustrativa de <?= $h1 ?></strong></div>
    </div>
    <div class="col-md-6">
        <div class="picture-legend picture-center"><a class="lightbox" href="<?= $url ?>imagens/transformadores/transformadores-02.jpg" title="<?= $h1 ?>" target="_blank"><img class="lazyload" data-src="<?= $url ?>imagens/transformadores/thumbs/transformadores-02.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>" /></a><strong>Imagem ilustrativa de <?= $h1 ?></strong></div>
    </div>
</div>