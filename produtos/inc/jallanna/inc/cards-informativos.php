
<section class="wrapper card-informativo">
    <span><p>O que nós fornecemos</p></span>
    <h2>Nossos principais focos</h2>
        <div class="cards-informativos">
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-map"></i>
            <h3>Missão</h3>
            <p>Fornecer transformadores de alta qualidade para máquinas de solda, com soluções confiáveis e personalizadas, garantindo a satisfação dos clientes e a excelência dos serviços.</p>
        </div>
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-eye"></i>
            <h3>Visão</h3>
        <p>Ser referência nacional no segmento de transformadores, destacando-se pela inovação, qualidade e fortalecimento de parcerias duradouras. </p>
    </div>
    <div class="cards-informativos-infos">
        <i class="fa-solid fa-handshake"></i>
        <h3>Valores</h3>
        <p>Qualidade, experiência, inovação, compromisso, profissionalismo e foco na satisfação dos clientes.</p>
    </div>
</div>
</section>
