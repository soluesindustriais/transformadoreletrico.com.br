
	<?php if (!$isMobile) : ?>
		<div class="slick-banner">
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.7) 50%, rgba(36, 36, 36, 0.7) 50%), url('<?= $url ?>imagens/banner/banner-1.webp')">
				<div>
					<div class="content-banner justify-content-evenly row">
						<div>
							<h1>Transformadores</h1>
							<p>Energia confiável para suas máquinas de solda.</p>
							<a class="btn" href="<?= $url ?>catalogo" title="Catalogo de Produtos">Clique</a>
						</div>
						<div>
							<img src="<?= $url ?>imagens/clientes/logo-categoria.png" alt="Logo da categoria" title="logo do cliente" class="slick-thumb">
						</div>
					</div>
				</div>
			</div>
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.5) 50%, rgba(36, 36, 36, 0.5) 50%), url('<?= $url ?>imagens/banner/banner-2.webp')">
				<div class="content-banner">
					<h2>Jal Lanna</h2>
					<p>Sua escolha para máquinas de solda de alta performance.</p>
					<a class="btn" href="<?= $url ?>sobre-nos" title="Página sobre nós">Clique</a>
				</div>
			</div>
	
		</div>
	<?php endif; ?>