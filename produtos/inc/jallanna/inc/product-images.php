<section class="product-container-image">
    <div class="product-image-aside">
    <?php
foreach ($menuItems as $value_products => $key_products) {
    if (isset($key_products["submenu"])) {
        $pathImgProduct = $key_products["submenu"][$h1]["url"];
        for($d = 1; $d < 3; $d++){
            if(file_exists($prefix_includes."imagens/informacoes/".$pathImgProduct."-$d.webp")){
                $pathImgProductInsert = "$pathImgProduct-$d.webp";
                echo '
        <div class="product-image-aside-item">
            <a href="'.$prefix_includes."imagens/informacoes/".$pathImgProductInsert.'" class="lightbox" title="'.$h1.'">
                <img class="product-image-aside-item-img" src="'.$prefix_includes."imagens/informacoes/".$pathImgProductInsert.'" alt="'.$h1.'">
            </a>
        </div>
                ';
            } else{
                $newpathImgProduct = "solucs.png";
                echo '
        <div class="product-image-aside-item">
            <a href="'.$prefix_includes."imagens/informacoes/".$newpathImgProduct.'" class="lightbox" title="'.$h1.'">
                <img class="product-image-aside-item-img" src="'.$prefix_includes."imagens/informacoes/".$newpathImgProduct.'" alt="'.$h1.'">
            </a>
        </div>
                ';
            }
        } 
    }
}

?>
    </div>
    <div class="product-image">
        <?php
        foreach ($menuItems as $value_products => $key_products) {
            if (isset($key_products["submenu"])) {
                $pathImgProductUse = $key_products["submenu"][$h1]["url"];
                if(file_exists($prefix_includes."imagens/informacoes/".$pathImgProductUse."-1.webp")){
                    $pathImgProductPrimeira = $prefix_includes."imagens/informacoes/".$pathImgProductUse."-1.webp";
                } else {
                    $pathImgProductPrimeira = $prefix_includes."imagens/informacoes/"."solucs.png";
                }
                echo '
                <a href="'.$pathImgProductPrimeira.'" class="lightbox" title="'.$h1.'">
                    <img id="img-principal" src="'.$pathImgProductPrimeira.'" alt="'.$h1.'">
                </a>
                ';
            }}
        ?>
    </div>
</section>

<script>
const imagens = document.querySelectorAll('.product-image-aside-item-img');
const principal_image = document.getElementById("img-principal");
imagens.forEach(function(img) {
    img.addEventListener('mouseover', function() {
        principal_image.src = this.src;
    });
});
</script>