<?php

if(!function_exists('generateSubmenu')) {
    function generateSubmenu($submenuItems, $url) {
        foreach($submenuItems as $itemName => $itemData): ?>
            <li>
                <a 
                    href="<?= strpos($itemData['url'], 'http') !== false ? $itemData['url'] : $url . $itemData['url'] ?>" 
                    title="<?= $itemName ?>"
                    <?= isset($itemData['target']) ? 'target="' . $itemData['target'] . '"' : '' ?>
                    <?= isset($itemData['rel']) ? 'rel="' . $itemData['rel'] . '"' : '' ?>
                >
                    <?= $itemName ?>
                </a>
                <?php if (isset($itemData['submenu'])): ?>
                    <ul class="sub-menu droppable">
                        <?php generateSubmenu($itemData['submenu'], $url); ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach;
    }
}

foreach ($menuItems as $itemName => $itemData):
    if($itemName === "SIG" && $useSigMenu === true){
        $useHamburgerMenu = true;
        include('inc/menu-top-inc.php');
    }
    else if($itemName !== "SIG") { ?>
        <li <?=isset($itemData['submenu']) ? "class='hamburger-dropdown'" : ""?>>
            <a 
                href="<?= strpos($itemData['url'], 'http') !== false ? $itemData['url'] : $link_minisite_subdominio . $itemData['url'] ?>" 
                title="<?= $itemName ?>"
                <?= isset($itemData['target']) ? 'target="' . $itemData['target'] . '"' : '' ?>
                <?= isset($itemData['rel']) ? 'rel="' . $itemData['rel'] . '"' : '' ?>
            >
                <?= $itemName ?>
            </a>
            <?php if (isset($itemData['submenu'])): ?>
                <ul class="<?= $itemName === 'Informações' ? 'sub-menu-info' : 'sub-menu' ?> droppable">
                    <?php if($itemName === "Informações"): 
                        include('inc/sub-menu.php');
                    else:
                        generateSubmenu($itemData['submenu'], $link_minisite); 
                    endif; ?>
                </ul>
            <?php endif; ?>
        </li>
    <?php }
endforeach; ?>
