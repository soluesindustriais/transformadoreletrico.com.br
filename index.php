<?
$h1         = 'Inversor';
$title      = 'Início - Inversor';
$desc       = 'Inversor - Conte com os melhores fabricantes de Inversores de Frequência do Brasil. É gratis!';  
$var        = 'Home';
include('inc/head.php');

?>
</head>

<body>
 <?php include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1>TRANSFORMADOR</h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>TRANSFORMADOR DE ENERGIA</h2>
        <p>O transformador de energia é crucial para a performace de qualquer empresa, por ter diversas funções e potências. Por obter núcleos que podem variar entre os de material ferromagnéticos e enrolamentos de cobre eletrolítico.</p>
        <a href="<?=$url?>transformador-de-energia" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>TRANSFORMADOR TOROIDAL</h2>
        <p>O transformador toroidal possui uma eficiência maior por conta do seu formato em anel. O equipamento não tem corte ou abertura em seu núcleo, que também é chamado de “gap”.</p>
        <a href="<?=$url?>transformador-toroidal" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>AUTO TRANSFORMADOR</h2>
        <p>Um transformador é um equipamento criado por Michael Faraday no início do século XIX. Sua principal função é transmitir uma potência elétrica de um circuito para o outro levando corrente, tensões e até mesmo modificando grandezas através de princípios estudados em eletromagnetismo.</p>
        <a href="<?=$url?>auto-transformador" class="cd-btn">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a title="next" href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a title="next" href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a title="next" href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main>
  <section class="wrapper-main">
    <div class="main-center">
      <div class=" quadro-2 ">
        <h2>TRANSFORMADORES</h2>
        <div class="div-img">
          <p>Toda empresa necessita de um planejamento adequado para prevenir possíveis interrupções e imprevistos, por essa razão é sempre importante contar com uma equipe especializada em solucionar problemas, assim esta tarefa se torna mais corriqueira e menos impactante na linha de produção industrial, esse processo é chamado de manutenção preventiva.</p>
        </div>
        <div class="gerador-svg">
          <img src="imagens/img-home/transformadores.png" alt="Inversor" title="Inversor">
        </div>
      </div>
      <div class=" incomplete-box">
        <ul>
          <li>
            <p>No caso da manutenção preventiva torre de resfriamento, por exemplo, a manutenção preventiva auxilia em uma série de fatores, como por exemplo:</p>
            <li><i class="fas fa-angle-right"></i>Reduzir riscos de quebra;</li>
            <li><i class="fas fa-angle-right"></i>Prevenir o envelhecimento e degeneração dos equipamentos;</li>
            <li><i class="fas fa-angle-right"></i>Programar a conversação das peças;</li>
            <li><i class="fas fa-angle-right"></i>Amenizar os custos de compra de novos itens.</li>
          </ul>
          <a href="<?=$url?>transformador-preco" class="btn-4">Saiba mais</a>
        </div>
      </div>
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons">
            <i class="fas fa-dollar-sign fa-7x"></i>
            <div>
              <p>COMPARE PREÇOS</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons">
            <i class="far fa-building fa-7x"></i>
            <div>
              <p>COTE COM DIVERSAS EMPRESAS</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons">
            <i class="far fa-handshake fa-7x"></i>
            <div>
              <p>FAÇA O MELHOR NEGÓCIO</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
       <div class="content-icons">

        <div class="produtos-relacionados-1">
          <figure>
            <div class="fig-img2">
            <a href="<?=$url?>transformador-de-energia">
              <h2>TRANSFORMADOR DE ENERGIA</h2>
            </a>
                <a href="<?=$url?>transformador-de-energia">
                <span class="btn-5"> Saiba Mais </span>
              </a>
              </div>
          </figure>
        </div>

        <div class="produtos-relacionados-2">
          <figure class="figure2">
              <div class="fig-img2">
            <a href="<?=$url?>transformador-a-seco">
                <h2>TRANSFORMADOR A SECO</h2>
            </a>
                <a href="<?=$url?>transformador-a-seco">
                <span class="btn-5"> Saiba Mais </span>
                </a>
              </div>
          </figure>
        </div>

        <div class="produtos-relacionados-3">
          <figure>
              <div class="fig-img2">
              <a href="<?=$url?>transformadores-de-potencia">
                <h2>TRANSFORMADORES DE POTÊNCIA</h2>
                </a>
                <a href="<?=$url?>transformadores-de-potencia">
                <span class="btn-5"> Saiba Mais </span>
                </a>
              </div>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/galeria-auto-transformador-eletrico.jpg" class="lightbox" title="Auto transformador elétrico">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-auto-transformador-eletrico.jpg" title="Auto transformador elétrico" alt="Auto transformador elétrico">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-distribuidor-de-autotransformador.jpg" class="lightbox"  title="Distribuidor de autotransformador">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-distribuidor-de-autotransformador.jpg" alt="Distribuidor de autotransformador" title="Distribuidor de autotransformador">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empresas-fabricantes-de-transformadores-de-potencia.jpg" class="lightbox" title="Empresas fabricantes de transformadores de potência">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empresas-fabricantes-de-transformadores-de-potencia.jpg" alt="Empresas fabricantes de transformadores de potência" title="Empresas fabricantes de transformadores de potência">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-fabricante-de-autotransformador.jpg" class="lightbox" title="Fabricante de autotransformador">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-fabricante-de-autotransformador.jpg" alt="Fabricante de autotransformador" title="Fabricante de autotransformador">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-preco-de-transformador.jpg" class="lightbox" title="Preço de transformador">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-preco-de-transformador.jpg" alt="Preço de transformador"  title="Preço de transformador">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-transformador-de-baixa-tensao.jpg" class="lightbox" title="Transformador de baixa tensão">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-transformador-de-baixa-tensao.jpg" alt="Transformador de baixa tensão" title="Transformador de baixa tensão">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-preco-de-transformador-trifasico.jpg" class="lightbox" title="Preço de transformador trifasico">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-preco-de-transformador-trifasico.jpg" alt="Preço de transformador trifasico" title="Preço de transformador trifasico">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-transformador-toroidal.jpg" class="lightbox" title="Transformador toroidal">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-transformador-toroidal.jpg" alt="Transformador toroidal" title="Transformador toroidal">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-transformadores-trifasicos.jpg" class="lightbox" title="Transformadores trifasicos">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-transformadores-trifasicos.jpg" alt="Transformadores trifasicos" title="Transformadores trifasicos">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-transformadores-eletricos.jpg" class="lightbox" title="Transformadores elétricos">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-transformadores-eletricos.jpg" alt="Transformadores elétricos" title="Transformadores elétricos">
              </a>
              </li>
          </ul>
        </div>
      </div>
    </section>
</main>
<?php include('inc/footer.php'); ?>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
</body>
</html>
