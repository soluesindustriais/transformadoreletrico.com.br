<? $h1 = "Transformador 220 para 380"; $title  = "Transformador 220 para 380"; $desc = "Receba uma estimativa de valor de $h1, ache as melhores empresas, cote produtos agora com mais de 200 indústrias de todo o Brasil"; $key  = "Comprar transformador 220 para 380,Transformadores 220 para 380"; include('inc/head.php');  ?>
</head>

<body>
     <?php include('inc/topo.php');?>
    <div class="wrapper">
        <main>
            <div class="content">
                <section><?=$caminhoinformacoes?><br class="clear" />
                    <h1><?=$h1?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?=$url?>imagens/mpi/transformador-220-para-380-01.jpg"
                                title="<?=$h1?>" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/transformador-220-para-380-01.jpg" title="<?=$h1?>"
                                    alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/transformador-220-para-380-02.jpg"
                                title="Comprar transformador 220 para 380" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/transformador-220-para-380-02.jpg"
                                    title="Comprar transformador 220 para 380"
                                    alt="Comprar transformador 220 para 380"></a><a
                                href="<?=$url?>imagens/mpi/transformador-220-para-380-03.jpg"
                                title="Transformadores 220 para 380" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/transformador-220-para-380-03.jpg"
                                    title="Transformadores 220 para 380" alt="Transformadores 220 para 380"></a></div>
                        <span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível
                            livremente na internet</span>
                        <hr />
                        <h2>Saiba o que é um transformador elétrico</h2>
                        <p>Os transformadores e <a style="color: #ff4d52;" href="<?=$url?>transformadores-eletricos">autotransformadores elétricos</a> são aplicados quando a tensão da rede
                            fornecida pela concessionária de energia ou pela cabine primária/subestação, localizada na
                            planta da edificação é diferente da tensão necessária para se ligar uma máquina, um
                            equipamento ou departamento.</p>
                        <h2>Principal uso do transformador 280 para 380</h2>
                        <p>O <strong>transformador 220 para 380</strong> tem a função de elevar a tensão elétrica de
                            220V para 380V. Ele é utilizado, por exemplo, quando uma rede é de 220V trifásico, mas
                            utiliza uma máquina que funciona com tensão 380V trifásico.</p>
                        <p>Existem transformadores especiais de 220V para 380V, que servem para converter redes
                            trifásicas em bifásicas ou monofásicas, o que protege a máquina do cliente contra distorções
                            harmônicas, além de realizar a isolação entre rede e carga.</p>
                        <h2>Tipos de transformadores</h2>
                        <p>Existem alguns tipos de transformadores, tais como:</p>
                        <ul>
                            <li class="li-mpi">Monofásico;</li>
                            <li class="li-mpi">Autotransformadores;</li>
                            <li class="li-mpi">Trifásicos (que funcionam em três fases: 220V – 380V – 440V).</li>
                        </ul>
                        <h2>Informações adicionais</h2>
                        <p>Para fazer com que essa máquina funcione com tensão 380V, será necessário um transformador
                            que faça mudança da tensão de 220V para 380V. O transformador é ideal, pois ao alimentá-lo
                            em 220 V trifásico, haverá 380V trifásico na saída.</p>
                        <p>O <strong>transformador 220 para 380</strong> é usado, sobretudo, na indústria e na
                            construção civil, para alimentar motores, extrusoras, injetoras, betoneiras, gruas, máquinas
                            gráficas e operatrizes, equipamentos de ar condicionado, entre outros. O Transformador pode
                            ser obtido por meio de empresas especializadas em transformadores, que indicarão se este é o
                            transformador certo para seu projeto.</p>
                    </article>
                     <?php include('inc/coluna-mpi.php');?><br class="clear">
                     <?php include('inc/busca-mpi.php');?>
                     <?php include('inc/form-mpi.php');?>
                     <?php include('inc/regioes.php');?>
                </section>
            </div>
        </main>
    </div>
     <?php include('inc/footer.php');?>
</body>

</html>